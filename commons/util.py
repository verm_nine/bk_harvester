import logging
import os
import configparser

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base

CONFIG_PATH = "../config.ini"

config = configparser.ConfigParser()


def setup_logger(name, level=logging.INFO):
    """The function setups as many loggers as you want"""

    config.read(CONFIG_PATH)
    log_folder = config.get('general', 'general_logs_directory')

    logger = logging.getLogger(name)
    logger.setLevel(level)

    # Create the Handler for logging data to a file
    try:
        logger_handler = logging.FileHandler(log_folder + name + '.log', encoding='utf-8')
    except FileNotFoundError:
        os.mkdir(log_folder)
        logger_handler = logging.FileHandler(log_folder + name + '.log', encoding='utf-8')
    logger_handler.setLevel(level)

    # Create a Formatter for formatting the log messages
    logger_formatter = logging.Formatter('%(asctime)s [%(levelname)s] %(module)s: %(message)s',
                                  datefmt='%m/%d/%Y %H:%M:%S')

    # Add the Formatter to the Handler
    logger_handler.setFormatter(logger_formatter)

    # Add the Handler to the Logger
    logger.addHandler(logger_handler)
    logger.info(f'Configuring logger {name} completed!')

    return logger


def create_config(config_path):
    """
    Create a config file
    """
    cwd = os.getcwd()
    parent_dir = os.path.dirname(cwd)
    config.add_section("general")
    config.set("general", "general_logs_directory", parent_dir+"\\log\\")
    config.set("general", "database_address", "sqlite:///"+parent_dir+"\\bk.db")

    with open(config_path, "w") as config_file:
        config.write(config_file)


if __name__ == "__main__":
    # Create config file if not exists.
    if not os.path.exists(CONFIG_PATH):
        print("No config file found. Creating one.")
        create_config(CONFIG_PATH)

    print("Setup is Done!")

