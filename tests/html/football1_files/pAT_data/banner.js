/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var $ = __webpack_require__(1);
$(document).ready(function () {
    var banner = new Banner(data);
    banner.run();
});
function debug() {
    var params = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        params[_i] = arguments[_i];
    }

}
var Banner = /** @class */ (function () {
    function Banner(data) {
        var _this = this;
        this.activeBanners = null;
        this.datesTimeout = 1 * 60 * 1000; // 1 min
        this.datesInterval = null;
        this.dataInterval = null;
        this.slickStarted = false;
        this.getUrl = function (html) {
            return '/public/b/' + _this.id + '/' + _this.version + '/p' + _this.countryCode + '.' + (html ? 'html' : 'json');
        };
        this.run = function () {
            var self = _this, banners = _this.getActiveBanners();
            if (banners.length == 0) {
                _this.postMessageRemove();
                return;
            }
            for (var i in banners) {
                var banner = banners[i];
                _this.container.find('#' + banner.getId()).bind('click', { model: banner }, _this.onClick);
            }
            _this.datesInterval = setInterval(function () { return self.datesRefresh(); }, _this.datesTimeout);
            _this.initDataInterval();
            _this.slickStart();
        };
        this.render = function () {
            var banners = _this.getActiveBanners(), elements = [];

            if (banners.length == 0) {
                _this.postMessageRemove();
                return;
            }
            for (var i in banners) {
                elements.push(_this.renderBanner(banners[i]));
            }
            _this.slickStop();
            _this.container.empty();
            _this.container.append(elements);
            _this.slickStart();
        };
        this.renderBanner = function (model) {

            var element = $("<div />"), img, wrapper, clickable;
            element
                .attr('id', model.getId())
                .addClass('banner')
                .addClass(model.getBannerType().toLowerCase())
                .addClass('size-' + model.getSizeId());
            if (Banner.BANNER_TYPE_HTML == model.getBannerType()) {
                img = $('<iframe />').attr({ src: model.getTargetUrl(), scrolling: 'no', frameborder: '0' });
            }
            else {
                img = $('<img />').attr('src', model.getImg());
                if (model.getAlt()) {
                    img.attr('alt', model.getAlt());
                }
            }
            clickable = true;
            if (!model.getTargetUrl() && (Banner.BANNER_TYPE_TOTO != model.getBannerType())) {
                clickable = false;
            }
            if (clickable) {
                wrapper = $('<a />').click(_this.onClick).attr({ 'href': _this.getUrl(true) });
            }
            else {
                wrapper = $('<div class="banner-wrapper" />');
            }
            wrapper.append(img);
            if (model.getData() != null) {
                if (model.getBannerType() == Banner.BANNER_TYPE_TOTO) {
                    var totoData = model.getData();
                    wrapper.append($('<div />')
                        .addClass('info')
                        .append($('<span />').addClass('currency').html(totoData['currency']))
                        .append($('<span />').addClass('jackpot').html(totoData['jackpot'])));
                }
            }
            element.append(wrapper);
            return element;
        };
        this.onClick = function (e) {
            e.preventDefault();
            var banner = e.data.model;
            // ignore not clickable
            if ((banner.getBannerType() == Banner.BANNER_TYPE_IMG && !banner.getTargetUrl()) ||
                (banner.getBannerType() == Banner.BANNER_TYPE_HTML)) {
                return;
            }
            var data = {
                action: banner.getAction(),
                type: banner.getBannerType(),
                url: banner.getTargetUrl(),
                statisticKey: banner.getStatisticKey()
            }, bannerData = banner.getData();
            _this.extend(data, bannerData);
            switch (banner.getAction()) {
                case Banner.ACTION_OPEN_IN_NEW_WINDOW:
                    data['newWindow'] = banner.getNewWindow();
                    break;
            }
            switch (banner.getBannerType()) {
                case Banner.BANNER_TYPE_TOTO:
                    delete data['url'];
                    delete data['currency'];
                    delete data['jackpot'];
                    break;
                case Banner.BANNER_TYPE_EVENT:
                    delete data['url'];
                    break;
            }
            _this.postMessage(data);
        };
        this.postMessageRemove = function () {
            var data = {
                action: Banner.ACTION_REMOVE
            };
            _this.postMessage(data);
        };
        this.postMessage = function (data) {

            if (!data['action']) {
                return;
            }
            data['id'] = _this.id;
            var message = {
                type: 'BANNER',
                data: data
            };
            if (window.addEventListener) {
                window.parent.postMessage(JSON.stringify(message), "*");
            }
            else {
                window.parent.postMessage(JSON.stringify(message), "*");
            }
        };
        this.setBanners = function (data) {
            _this.banners = [];
            for (var i in data) {
                _this.banners.push(new BannerDefinitionModel(data[i]));
            }
            _this.activeBanners = null;
        };
        this.getActiveBanners = function (force) {
            if (force === void 0) { force = false; }
            if ((_this.activeBanners == null) || force) {
                var activeBanners = [], currentTime = new Date().getTime();
                for (var i in _this.banners) {
                    var banner = _this.banners[i], dateFrom = banner.getDateFrom() || currentTime, dateTo = banner.getDateTo() || currentTime;
                    if (!((dateFrom <= currentTime) && (dateTo >= currentTime))) {

                        continue;
                    }
                    activeBanners.push(banner);
                }
                _this.activeBanners = activeBanners;
            }
            return _this.activeBanners;
        };
        this.slickStart = function () {
            var banners = _this.getActiveBanners(), started = banners.length > 1;
            if (started) {
                _this.container.slick(_this.getSlickSettings());
                _this.container.slick('goTo', 0, true);
                _this.slickStarted = true;
            }

        };
        this.getSlickSettings = function () {
            var settings = {
                speed: 1000,
                autoplay: true,
                autoplaySpeed: _this.settings[Banner.SETTINGS_ROTATE_TIMEOUT] * 1000,
                arrows: false,
                dots: _this.isShowDots()
            };
            if (Banner.SITE_PART_CODE_RIGHT_COLUMN == _this.sitePartCode) {
                settings['fade'] = true;
                settings['cssEase'] = 'linear';
            }
            return settings;
        };
        this.slickStop = function () {

            if (_this.slickStarted) {
                _this.slickStarted = false;
                _this.container.slick('unslick');
            }
        };
        this.slickChangeRotate = function () {

            _this.container.slick('slickPause');
            _this.container.slick('slickSetOption', 'autoplaySpeed', _this.settings[Banner.SETTINGS_ROTATE_TIMEOUT] * 1000);
            _this.container.slick('slickPlay');
        };
        this.datesRefresh = function () {
            var currentData = _this.getActiveBanners(), newData = _this.getActiveBanners(true), changed = false;
            if (currentData.length == newData.length) {
                for (var i = 0; i < currentData.length; i++) {
                    if (currentData[i].getDateFrom() !== newData[i].getDateFrom()) {
                        changed = true;
                        break;
                    }
                    if (currentData[i].getDateTo() !== newData[i].getDateTo()) {
                        changed = true;
                        break;
                    }
                }
            }
            else {
                changed = true;
            }
            if (changed) {

                _this.render();
            }
        };
        this.initDataInterval = function () {
            var self = _this;
            if (_this.dataInterval) {
                clearInterval(_this.dataInterval);
            }
            _this.dataInterval = setInterval(function () { return self.dataRefresh(); }, _this.settings[Banner.SETTINGS_DATA_REFRESH_TIMEOUT] * 1000);

        };
        this.dataRefresh = function () {
            var self = _this;
            $.ajax({
                url: _this.getUrl(false),
                type: 'GET',
                success: function (response) {
                    if (!response) {
                        return;
                    }
                    if (response['settings']) {
                        var settings = response['settings'], changes = {};
                        for (var key in self.settings) {
                            if (self.settings[key] != settings[key]) {
                                changes[key] = true;
                            }
                        }
                        self.settings = settings;
                        for (var key in changes) {
                            switch (key) {
                                case Banner.SETTINGS_DATA_REFRESH_TIMEOUT:
                                    self.initDataInterval();
                                    break;
                                case Banner.SETTINGS_ROTATE_TIMEOUT:
                                    self.slickChangeRotate();
                                    break;
                            }
                        }
                    }
                    if (response['banners']) {

                        self.version = response['version'];
                        self.setBanners(response['banners']);
                        self.render();
                    }
                },
                error: function () {

                }
            });
        };
        this.isShowDots = function () {
            return Banner.SITE_PART_CODE_HOME_PAGE == _this.sitePartCode;
        };
        this.id = data['id'];
        this.countryCode = data['countryCode'];
        this.version = data['version'];
        this.settings = data['settings'];
        this.sitePartCode = data['sitePartCode'];
        this.setBanners(data['banners']);
        this.container = $('.banners');

    }
    Banner.prototype.extend = function (destination, source) {
        if (!source) {
            return;
        }
        for (var field in source) {
            destination[field] = source[field];
        }
    };
    Banner.ACTION_OPEN_IN_NEW_WINDOW = 'OPEN_IN_NEW_WINDOW';
    Banner.ACTION_REMOVE = 'REMOVE';
    Banner.BANNER_TYPE_IMG = 'IMG';
    Banner.BANNER_TYPE_EVENT = 'EVENT';
    Banner.BANNER_TYPE_HELP = 'HELP';
    Banner.BANNER_TYPE_TOTO = 'TOTO';
    Banner.BANNER_TYPE_HTML = 'HTML';
    Banner.SETTINGS_DATA_REFRESH_TIMEOUT = 'dataRefreshTimeout';
    Banner.SETTINGS_ROTATE_TIMEOUT = 'rotateTimeout';
    Banner.SITE_PART_CODE_RIGHT_COLUMN = 'RIGHT_COLUMN';
    Banner.SITE_PART_CODE_HOME_PAGE = 'HOME_PAGE';
    return Banner;
}());
var BannerDefinitionModel = /** @class */ (function () {
    function BannerDefinitionModel(obj) {
        this.id = obj['id'];
        this.action = obj['action'];
        this.bannerType = obj['bannerType'];
        this.img = obj['img'];
        this.targetUrl = obj['targetUrl'];
        this.alt = obj['alt'];
        this.dateFrom = obj['dateFrom'];
        this.dateTo = obj['dateTo'];
        this.sizeId = obj['sizeId'];
        this.statisticKey = obj['statisticKey'];
        this.data = obj['data'];
        this.newWindow = obj['newWindow'];
    }
    BannerDefinitionModel.prototype.getId = function () {
        return this.id;
    };
    BannerDefinitionModel.prototype.getAction = function () {
        return this.action;
    };
    BannerDefinitionModel.prototype.getBannerType = function () {
        return this.bannerType;
    };
    BannerDefinitionModel.prototype.getImg = function () {
        return this.img;
    };
    BannerDefinitionModel.prototype.getTargetUrl = function () {
        return this.targetUrl;
    };
    BannerDefinitionModel.prototype.getAlt = function () {
        return this.alt;
    };
    BannerDefinitionModel.prototype.getDateFrom = function () {
        return this.dateFrom;
    };
    BannerDefinitionModel.prototype.getDateTo = function () {
        return this.dateTo;
    };
    BannerDefinitionModel.prototype.getSizeId = function () {
        return this.sizeId;
    };
    BannerDefinitionModel.prototype.getData = function () {
        return this.data;
    };
    BannerDefinitionModel.prototype.getStatisticKey = function () {
        return this.statisticKey;
    };
    BannerDefinitionModel.prototype.getNewWindow = function () {
        return this.newWindow;
    };
    return BannerDefinitionModel;
}());


/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = $;

/***/ })
/******/ ]);