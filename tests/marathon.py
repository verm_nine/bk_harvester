import datetime
import unittest
from unittest.mock import Mock

import os

from harvester.marathon import Marathon
from model.vo import EventVO, MatchVO, MatchOccurrenceVO

DT_NOW = datetime.datetime(2019, 4, 15, 13, 29, 22, 714606)

MATCH_ID = 1000


class TestClientServer(unittest.TestCase):

    def setUp(self):
        # Relative path is used for the config file. So go one level above.
        os.chdir("tests")

    def tearDown(self):
        os.chdir("..")

    def test_parse_live_football1__data_fine__return_fine(self):
        m = Marathon()
        m.driver.get('file:///' + os.getcwd() +'/html/marathon_football1.html')
        # MatchVO to be found and parsed
        matchVO = MatchVO(kind_of_sport="футбол",
                          championship="Австралия. Национальная Премьер-лига. Виктория",
                          team1="Кингстон Сити",
                          team2="Грин Галли",
                          bk="marathon",
                          quality=0,
                          events_amount=9000,  # fix me (count me before fix)
                          start_date=None,  # cannot be gathered from live
                          last_update=DT_NOW,
                          id=MATCH_ID,
                          orig_time="80:30",
                          std_time="80:30;2;",
                          http_link="https://www.marathonbet.com/su/live/7990648",
                          live_status='live',
                          live_score='0:4(0:3;0:1)',
                          events=None)


        m.driver.get = Mock()  # mock page retrieval func so it can't overwrite local html file

        res = m.parse_live(matchVO)

        # Events to be found
        es = []
        es.append(EventVO(original_event="Кингстон Сити(+3.5)", standardized_event="Ф1(3.5)", odds=5.05,
                          live=True, stopped=False, match_id=MATCH_ID,
                          description="Победа с учетом форы", last_update=DT_NOW))
        es.append(EventVO(original_event="Грин Галли(-4.5)", standardized_event="Ф2(-4.5)", odds=3.78,
                          live=True, stopped=False, match_id=MATCH_ID,
                          description="Победа с учетом форы", last_update=DT_NOW))

        es.append(EventVO(original_event="Меньше(4.5)", standardized_event="ТМ(4.5)", odds=1.68,
                          live=True, stopped=False, match_id=MATCH_ID,
                          description="Тотал голов", last_update=DT_NOW))
        # one total in the main column, another is below, both should be grabbed
        es.append(EventVO(original_event="Меньше(4.5)", standardized_event="ТМ(4.5)", odds=1.68,
                          live=True, stopped=False, match_id=MATCH_ID,
                          description="", last_update=DT_NOW))
        es.append(EventVO(original_event="Больше(5.5)", standardized_event="ТБ(5.5)", odds=6.30,
                          live=True, stopped=False, match_id=MATCH_ID,
                          description="Тотал голов", last_update=DT_NOW))

        es.append(EventVO(original_event="Меньше(2.0)", standardized_event="ТМ(2) 2Т", odds=1.15,
                          live=True, stopped=False, match_id=MATCH_ID,
                          description="Тотал голов, 2-й тайм", last_update=DT_NOW))
        es.append(EventVO(original_event="Меньше(0.5)", standardized_event="ИТМ1(0.5)", odds=1.23,
                          live=True, stopped=False, match_id=MATCH_ID,
                          description="Тотал голов (Кингстон Сити), 2-й тайм", last_update=DT_NOW))
        es.append(EventVO(original_event="Больше(2.0)", standardized_event="ИТБ(2)", odds=9.90,
                          live=True, stopped=False, match_id=MATCH_ID,
                          description="Тотал голов (Грин Галли), 2-й тайм", last_update=DT_NOW))
        for e in es:
            assert e in res[0].events
