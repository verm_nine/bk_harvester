import abc
import logging
import selectors
import socket

from commons.util import setup_logger
from datetime import datetime
from model.vo import EventVO, MatchVO, MatchOccurrenceVO
prepare_request = lambda arg: None
from selenium import webdriver

PREFIX_EVENTS_MATCHES = b"MATCHE"
PREFIX_EVENTS_OCCURENCES = b"OCCURE"


class Harvester(metaclass=abc.ABCMeta):

    def __init__(self):
        # Net settings.
        self.sel = selectors.DefaultSelector()
        self.host = '127.0.0.1'
        self.port = 61002

        # Selenium settings.
        options = webdriver.ChromeOptions()
        # options.add_argument('headless')
        options.binary_location = r"..\bin\chrome-win\chrome.exe"
        self.driver = webdriver.Chrome(executable_path=r'..\bin\chromedriver.exe', chrome_options=options)
        self.driver.maximize_window()

        # Setting up logger here, so less problems with tests.
        self.logger = setup_logger("harvester", level=logging.DEBUG)


    def send_data(self, message):
        server_addr = (self.host, self.port)
        self.logger.info(f"starting connection to {server_addr}")
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setblocking(False)
        sock.connect_ex(server_addr)
        net_events = selectors.EVENT_READ | selectors.EVENT_WRITE
        message = prepare_request(message)
        self.sel.register(sock, net_events, data=message)

        net_events = self.sel.select(timeout=1)
        try:
            if net_events:
                for key, mask in net_events:
                    sock = key.fileobj
                    data = key.data
                    if mask & selectors.EVENT_READ:
                        recv_data = sock.recv(1024)  # Should be ready to read data from the server.
                        if recv_data:
                            self.logger.info(f"received data from the server: {repr(recv_data)}")
                        if not recv_data:
                            continue
                    if mask & selectors.EVENT_WRITE:
                        self.logger.debug(f"sending {repr(data)}")
                        sent = sock.send(message)  # Should be ready to write to the server.
                        self.logger.info(f"sent {sent} bytes")
            else:
                # nothing has been sent, as the server hasn't opened a socket
                raise OSError("Socket is not connected")

        except OSError as err:
            self.logger.error(f"no connection to {server_addr}! {err}")
        finally:
            self.logger.info(f"closing connection to {server_addr}")
            self.sel.unregister(sock)
            sock.close()

    @abc.abstractmethod
    def parse_prelive(self):
        """Parses all the available pre-live matches for the
        next 7 days and today.
        In the case if bookie has no pre-live betting line
        do nothing. Assumed, this method will be called once daily
        to update data. Doesn't send data itself.
        :returns: list: contains MatchVO objects"""
        pass

    @abc.abstractmethod
    def parse_live(self, match, until_ends=False):
        """Parses a good match for available events. Sends data itself.
        :param match: (MatchVO) will be used to find the match
        :param until_ends: (bool) if True sends data to the server
                           using send_events each second until the
                           match ends.
        :returns: list with one MatchVO object
        """
        pass

    def send_matches(self, matches):
        """Sends matches (which contain related events) to the server.
        :param matches: (list of MatchVO) list of the parsed matches with its events
        :returns: None"""
        # Serialize matches. Transform to JSON.
        matches_json = MatchVO.schema().dumps(matches, many=True)

        self.logger.info(f"attempt to send <{len(matches)}> matches to the server")
        self.send_data(PREFIX_EVENTS_MATCHES + matches_json.encode('utf-8'))

    def send_match_occurrences(self, occurrences):
        """Sends occurrences for a match (which is specified inside MatchOccurrenceVO).
        :param occurrences: (list of MatchOccurrenceVO) list of the caught occurences
        during a match (breaks, bet stops)
        :returns: None"""
        # Serialize occurrences. Transform to JSON.
        matches_json = MatchOccurrenceVO.schema().dumps(occurrences, many=True)

        self.logger.info(f"attempt to send <{len(occurrences)}> match occurrences to the server")
        self.send_data(PREFIX_EVENTS_OCCURENCES + matches_json.encode('utf-8'))


if __name__ == '__main__':
    class TestHarvester(Harvester):

        def parse_prelive(self):
            pass

        def parse_live(self, match_name, until_ends=False):
            pass

    h = TestHarvester()
    e1 = EventVO(original_event="Ф Борисов (100)", standardized_event="Ф1(100)", odds=3.3, live=False, stopped=False,
                 last_update=datetime.utcnow())
    e2 = EventVO(original_event="всей херни Тотал меньше 444.5", standardized_event="ТМ(444.5)", odds=31.3, live=False,
                 stopped=False,
                 last_update=datetime.utcnow())
    events = [e1, e2]
    m1 = MatchVO(kind_of_sport="kind_of_sport1", championship="leagua meshko1v", team1="bar11is", team2="bor33is",
                 bk="1xbet", quality=3.30, events_amount=2, id=-1, start_date=datetime.utcnow(), events=events,
                 last_update=datetime.utcnow())
    m2 = MatchVO(kind_of_sport="kind_of_sport2", championship="leagdua meshkov", team1="barddis", team2="boridds",
                 bk="1xbet", quality=31.3, events_amount=44444, id=-1, start_date=datetime.utcnow(), events=events,
                 last_update=datetime.utcnow())
    matches_to_send = [m1, m2]
    h.send_matches(matches_to_send)

    o1 = MatchOccurrenceVO(1, "31:42 3П", "3П 7:42", "0:0(0:0;0:0;0:0)", "break", datetime.utcnow())
    o2 = MatchOccurrenceVO(1, "29:42 3П", "3П 5:42", "0:0(0:0;0:0;0:0)", "stop_betting", datetime.utcnow())
    h.send_match_occurrences([o1, o2])
