from datetime import datetime
from typing import List

from dataclasses import dataclass, field
from dataclasses_json import dataclass_json


@dataclass_json
@dataclass
class EventVO:
    """Value object that used for data transfer from a Harvester to the Server.
    Used inside MatchVO"""
    original_event: str
    standardized_event: str
    odds: float
    live: bool  # live and non-live limits differ drastically
    stopped: bool  # if bet accepting temporarily stopped
    last_update: datetime
    match_id: int = -1
    description: str = ''
    limit: int = -1
    currency: str = 'rub'


@dataclass_json
@dataclass
class MatchVO:
    kind_of_sport: str
    championship: str
    team1: str
    team2: str
    bk: str
    quality: int
    events_amount: int
    start_date: datetime
    last_update: datetime
    id: int = -1
    orig_time: str = ""  # match time how it is presented on a bookie
    std_time: str = ""  # std match time for this kind of sport, ex: "37:44;4;BREAK"
    http_link: str = ""  # http link to the match
    live_status: str = 'non-live'  # if live then describes if break or delay is present
    live_score: str = ''  # format differs for each kind of sport, ex: "1:0(6:2;1:0)30:A"
    events: List[EventVO] = field(default_factory=list)


@dataclass_json
@dataclass
class MatchOccurrenceVO:
    """Value object that used to describe something that happened during a match
    like a break."""
    match_id: int
    orig_time: str
    std_time: str
    live_score: str
    type: str
    last_update: datetime
